<?php

namespace App\Http\Controllers;

use Mail;
use Config;
use Illuminate\Http\Request;

class ContactController extends Controller
{
	private $request = null;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * send an email from contact form
	 *
	 * @return Message - normally this would be called inside an AJAX request and return a JSON result
	 */
	public function send()
	{
		$this->validate($this->request, [
			'name' => 'required',
			'surname' => 'required',
			'email' => 'required|email',
			'user_message' => 'required',
			'photo' => 'image'
		]);

		//send email from contact form
		$sent = $this->sendMessage();

		//send thank-you message to sender
		$this->sendThankyou();

		if($sent)
			echo trans('messages.thank_you');
		else
			echo trans('messages.whoops');
	}

	private function sendMessage()
	{
		return Mail::send('contact_form_email', $this->request->all(), function($message) {

			//attachment
			if ($this->request->hasFile('photo') && $this->request->file('photo')->isValid())
				$message->attach(
					$this->request->photo->getRealPath(),
					array(
						'as' => $this->request->photo->getClientOriginalName()
					)
				);

			//from address
			$message->from($this->request->input('email'), $this->request->input('name'));

			//to address
			$message->to(Config::get('mail.from.address'), Config::get('mail.from.name'));

			//subject
			$message->subject(trans('messages.email_subject'));
		});
	}

	private function sendThankyou()
	{
		return Mail::send('thankyou_email', $this->request->all(), function($message) {

			//to address
			$message->to($this->request->input('email'), $this->request->input('name'));

			//subject
			$message->subject(trans('messages.thankyou_subject'));
		});
	}
}